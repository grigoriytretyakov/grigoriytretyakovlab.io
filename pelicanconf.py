#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'\u0413\u0440\u0438\u0433\u043e\u0440\u0438\u0439'
SITENAME = u'Just a blog!'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Moscow'

DEFAULT_LANG = u'ru'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

THEME = 'themes/first'

STATIC_PATHS = ['projects', '404.html']

DEFAULT_PAGINATION = 10

DISPLAY_PAGES_ON_MENU = True

# это чтобы html-файлы не парсились
READERS = {'html': None}
